import "./App.scss";
import SideNavbar from "./components/SideNavbar/SideNavbar";
import BuyCard from "./components/BuyCard/BuyCard";
import Modal from "./components/Modal/Modal";
import { useState, useRef, useEffect } from "react";
import listenForOutsideClick from "./listenForOutSideClick";
import EstimateModal from "./components/EstimateModal/EstimateModal";
import PaymentMethodModal from "./components/PaymentMethodModal/PaymentMethodModal.jsx";
import CustomPack from "./components/CustomPack/CustomPack";
import History from "./components/History/History";
import AddCard from "./components/AddCard/AddCard";
import BillingInformationModal from "./components/BillingInformationModal/BillingInformationModal";

const App = (props) => {
  const [showHideModal, setShowHideModal] = useState(false);
  const [listening, setListening] = useState(false);
  const [card, setCard] = useState(true);
  const [modalState, setModalState] = useState("paymentMethod");
  const modalRef = useRef(null);

  useEffect(() =>
    listenForOutsideClick(listening, setListening, modalRef, setShowHideModal)
  );

  const handleShowModal = (event, value) => {
    event.stopPropagation();
    setModalState(value);
    setShowHideModal(true);
  };

  const closeHandler = (event) => {
    setShowHideModal(false);
  };

  //   const PaymentMethodModal = () => {
  //     return (
  //       <div>
  //         <div className="modal-arrow-heading" onClick={closeHandler}>
  //           <div>
  //             <svg
  //               width="6"
  //               height="12"
  //               viewBox="0 0 6 12"
  //               fill="none"
  //               xmlns="http://www.w3.org/2000/svg"
  //             >
  //               <path
  //                 d="M5.53116 11.2871C5.8736 10.9935 5.91326 10.478 5.61974 10.1355L2.07529 6.00034L5.61974 1.86515C5.91326 1.5227 5.8736 1.00714 5.53116 0.713611C5.18871 0.420083 4.67314 0.459742 4.37962 0.802191L0.379615 5.46886C0.117472 5.77469 0.117472 6.22598 0.379615 6.53182L4.37962 11.1985C4.67314 11.5409 5.18871 11.5806 5.53116 11.2871Z"
  //                 fill="#0B081E"
  //                 stroke="#0B081E"
  //                 stroke-width="0.3"
  //               />
  //             </svg>
  //           </div>
  //           <div>
  //             <h2>Payment Method</h2>
  //           </div>
  //         </div>
  //         <div>
  //           <div className="payment-modal-saved-card">
  //             <div>
  //               <div className="small-gray-text">Mastercard</div>
  //               <div>4567</div>
  //             </div>
  //             <div>
  //               <div className="small-gray-text">Valid thru</div>
  //               <div>08/28</div>
  //             </div>
  //             <button className="payment-modal-default">Default</button>
  //           </div>
  //           <div className="add-payment-method">Add Payment Method</div>
  //           <hr />
  //           <div className="billing-information">
  //             <h3>Billing Information</h3>
  //             <hr />
  //             <p>
  //               1600 Amphitheatre Parkway Montain View, CA 94043 United States
  //             </p>
  //             <div className="small-gray-text">
  //               Shipping Address Same as Billing Address
  //             </div>
  //             <div className="update-information-container">
  //               <div>
  //                 <svg
  //                   width="16"
  //                   height="18"
  //                   viewBox="0 0 16 18"
  //                   fill="none"
  //                   xmlns="http://www.w3.org/2000/svg"
  //                 >
  //                   <path
  //                     fill-rule="evenodd"
  //                     clip-rule="evenodd"
  //                     d="M0 15.5V12.5C0 12.3674 0.0526784 12.2402 0.146447 12.1464L12.1464 0.146447C12.3417 -0.0488155 12.6583 -0.0488155 12.8536 0.146447L15.8536 3.14645C16.0488 3.34171 16.0488 3.65829 15.8536 3.85355L3.85355 15.8536C3.75979 15.9473 3.63261 16 3.5 16H0.5C0.223858 16 0 15.7761 0 15.5ZM3.29289 15L14.7929 3.5L12.5 1.20711L1 12.7071V15H3.29289ZM0 17.5C0 17.7761 0.223858 18 0.5 18H10.5069C10.7831 18 11.0069 17.7761 11.0069 17.5C11.0069 17.2239 10.7831 17 10.5069 17H0.5C0.223858 17 0 17.2239 0 17.5Z"
  //                     fill="#0B081E"
  //                   />
  //                 </svg>
  //               </div>
  //               <div>Update Information</div>
  //             </div>
  //           </div>
  //         </div>
  //       </div>
  //     );
  //   };

  //   const CustomPack = () => {
  //     return (
  //       <div>
  //         <div className="modal-arrow-heading">
  //           <div>
  //             <svg
  //               width="6"
  //               height="12"
  //               viewBox="0 0 6 12"
  //               fill="none"
  //               xmlns="http://www.w3.org/2000/svg"
  //             >
  //               <path
  //                 d="M5.53116 11.2871C5.8736 10.9935 5.91326 10.478 5.61974 10.1355L2.07529 6.00034L5.61974 1.86515C5.91326 1.5227 5.8736 1.00714 5.53116 0.713611C5.18871 0.420083 4.67314 0.459742 4.37962 0.802191L0.379615 5.46886C0.117472 5.77469 0.117472 6.22598 0.379615 6.53182L4.37962 11.1985C4.67314 11.5409 5.18871 11.5806 5.53116 11.2871Z"
  //                 fill="#0B081E"
  //                 stroke="#0B081E"
  //                 stroke-width="0.3"
  //               />
  //             </svg>
  //           </div>
  //           <div>
  //             <h2>CustomPack Pack Request</h2>
  //           </div>
  //         </div>
  //         <hr />
  //         <div className="regular-input-container">
  //           <label>Approx. Credits Needed</label>
  //           <input
  //             type="text"
  //             className="regular-modal-input"
  //             placeholder="56789"
  //           />
  //         </div>
  //         <div className="regular-input-container">
  //           <label>Approx. Storage Needed</label>
  //           <input
  //             type="text"
  //             className="regular-modal-input"
  //             placeholder="3000GB"
  //           />
  //         </div>
  //         <div className="add-card-button">Submit</div>
  //       </div>
  //     );
  //   };

  const renderModal = () => {
    switch (modalState) {
      case "paymentMethod":
        return (
          <PaymentMethodModal
            close={closeHandler}
            changeModalContent={handleShowModal}
          />
        );
      case "customPack":
        return <CustomPack close={closeHandler}  changeModalContent={handleShowModal}/>;
      case "estimateModal":
        return <EstimateModal close={closeHandler} />;
      case "purchaseHistory":
        return <History close={closeHandler} />;
      case "usageHistory":
        return <History close={closeHandler} />;
      case "addCard":
        return (
          <AddCard
            close={closeHandler}
            changeModalContent={handleShowModal}
            isCardEmpty={card}
          />
        );
      case "billingInformation":
        return <BillingInformationModal changeModalContent={handleShowModal} />;
      case 'requestCustom':
        return <div className="thank-you-modal-message">
                  <h2>Thank you!</h2>
                  <h2>We have successfully recevied your request. One of our executive will reach you out</h2>
            </div>
      default:
        return null;
    }
  };

  return (
    <div className="app" onClick={(event) => closeHandler(event)}>
      <SideNavbar />
      <div
        className="billing-section-container"
        style={{ opacity: showHideModal && 0.5 }}
      >
        <div className="payment-section-container">
          <div className="payment-section-top-container">
            <div
              className="credit-used-available-card"
              onClick={(event) => handleShowModal(event, "purchaseHistory")}
            >
              <div>
                <div className="small-gray-text">Credits Available</div>
                <div className="available-credit-count">2901</div>
              </div>
              <div className="history-container">
                <div style={{ color: "#000" }} className="history-text">
                  Purchase History
                </div>
                <svg
                  width="4"
                  height="6"
                  viewBox="0 0 4 6"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M2.56033 2.99968L0.746269 0.883278C0.626462 0.743502 0.642649 0.533069 0.782425 0.413262C0.9222 0.293454 1.13263 0.309641 1.25244 0.449417L3.25244 2.78275C3.35944 2.90758 3.35944 3.09178 3.25244 3.21661L1.25244 5.54994C1.13263 5.68972 0.9222 5.70591 0.782425 5.5861C0.642649 5.46629 0.626462 5.25586 0.746269 5.11608L2.56033 2.99968Z"
                    fill="#0B081E"
                  />
                </svg>
              </div>
            </div>
            <div
              className="credit-used-available-card"
              onClick={(event) => handleShowModal(event, "usageHistory")}
            >
              <div>
                <div className="small-gray-text">Credits Used</div>
                <div className="used-credit-count">3804</div>
              </div>
              <div className="history-container">
                <div style={{ color: "#000" }} className="history-text">
                  Usage History
                </div>
                <svg
                  width="4"
                  height="6"
                  viewBox="0 0 4 6"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M2.56033 2.99968L0.746269 0.883278C0.626462 0.743502 0.642649 0.533069 0.782425 0.413262C0.9222 0.293454 1.13263 0.309641 1.25244 0.449417L3.25244 2.78275C3.35944 2.90758 3.35944 3.09178 3.25244 3.21661L1.25244 5.54994C1.13263 5.68972 0.9222 5.70591 0.782425 5.5861C0.642649 5.46629 0.626462 5.25586 0.746269 5.11608L2.56033 2.99968Z"
                    fill="#0B081E"
                  />
                </svg>
              </div>
            </div>
            {card ? (
              <div
                className="saved-card"
                style={{ color: "#000", display: "flex" }}
              >
                <div className="card-detailes-container">
                  <svg
                    width="48"
                    height="34"
                    viewBox="0 0 48 34"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fill-rule="evenodd"
                      clip-rule="evenodd"
                      d="M0.666016 6.50033C0.666016 3.27866 3.27769 0.666992 6.49935 0.666992H41.4993C44.721 0.666992 47.3327 3.27866 47.3327 6.50033V27.5003C47.3327 30.722 44.721 33.3337 41.4993 33.3337H6.49935C3.27769 33.3337 0.666016 30.722 0.666016 27.5003V6.50033ZM44.9993 6.50032V7.66699H2.99935V6.50032C2.99935 4.56733 4.56635 3.00033 6.49935 3.00033H41.4993C43.4323 3.00033 44.9993 4.56733 44.9993 6.50032ZM44.9993 12.3337V10.0003H2.99935V12.3337H44.9993ZM44.9993 14.667H2.99935V27.5003C2.99935 29.4333 4.56635 31.0003 6.49935 31.0003H41.4993C43.4323 31.0003 44.9993 29.4333 44.9993 27.5003V14.667ZM35.6663 28.0428C33.8685 29.083 31.5479 28.8152 30.0329 27.3002C28.2104 25.4777 28.2104 22.5229 30.0329 20.7005C31.5454 19.1879 33.8651 18.9158 35.6663 19.9578C36.3526 19.5609 37.1494 19.3337 37.9994 19.3337C40.5767 19.3337 42.666 21.423 42.666 24.0003C42.666 26.5777 40.5767 28.667 37.9994 28.667C37.1495 28.667 36.3527 28.4398 35.6663 28.0428ZM33.3327 24.0003C33.3327 24.8203 33.5442 25.5909 33.9156 26.2606C33.1395 26.4598 32.2839 26.2514 31.6828 25.6502C30.7715 24.739 30.7715 23.2616 31.6828 22.3504C32.2832 21.75 33.1387 21.5406 33.9156 21.74C33.5442 22.4097 33.3327 23.1803 33.3327 24.0003ZM40.3327 24.0003C40.3327 25.289 39.288 26.3337 37.9993 26.3337C36.7107 26.3337 35.666 25.289 35.666 24.0003C35.666 22.7117 36.7107 21.667 37.9993 21.667C39.288 21.667 40.3327 22.7117 40.3327 24.0003Z"
                      fill="#0B081E"
                    />
                  </svg>

                  <div>
                    <div className="large-gray-text">Mastercard</div>
                    <div style={{ fontWeight: "400", fontSize: "1.4rem" }}>
                      4567
                    </div>
                  </div>
                  <div>
                    <div className="large-gray-text">Valid thru</div>
                    <div style={{ fontWeight: "400", fontSize: "1.4rem" }}>
                      08/28
                    </div>
                  </div>
                </div>
                <button
                  type="button"
                  className="payment-method-button"
                  onClick={(event) => handleShowModal(event, "paymentMethod")}
                >
                  Payment Method
                </button>
              </div>
            ) : (
              <div
                className="saved-card"
                style={{ color: "#000", display: "flex" }}
              >
                <div className="card-detailes-container">
                  <svg
                    width="48"
                    height="34"
                    viewBox="0 0 48 34"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fill-rule="evenodd"
                      clip-rule="evenodd"
                      d="M0.666016 6.50033C0.666016 3.27866 3.27769 0.666992 6.49935 0.666992H41.4993C44.721 0.666992 47.3327 3.27866 47.3327 6.50033V27.5003C47.3327 30.722 44.721 33.3337 41.4993 33.3337H6.49935C3.27769 33.3337 0.666016 30.722 0.666016 27.5003V6.50033ZM44.9993 6.50032V7.66699H2.99935V6.50032C2.99935 4.56733 4.56635 3.00033 6.49935 3.00033H41.4993C43.4323 3.00033 44.9993 4.56733 44.9993 6.50032ZM44.9993 12.3337V10.0003H2.99935V12.3337H44.9993ZM44.9993 14.667H2.99935V27.5003C2.99935 29.4333 4.56635 31.0003 6.49935 31.0003H41.4993C43.4323 31.0003 44.9993 29.4333 44.9993 27.5003V14.667ZM35.6663 28.0428C33.8685 29.083 31.5479 28.8152 30.0329 27.3002C28.2104 25.4777 28.2104 22.5229 30.0329 20.7005C31.5454 19.1879 33.8651 18.9158 35.6663 19.9578C36.3526 19.5609 37.1494 19.3337 37.9994 19.3337C40.5767 19.3337 42.666 21.423 42.666 24.0003C42.666 26.5777 40.5767 28.667 37.9994 28.667C37.1495 28.667 36.3527 28.4398 35.6663 28.0428ZM33.3327 24.0003C33.3327 24.8203 33.5442 25.5909 33.9156 26.2606C33.1395 26.4598 32.2839 26.2514 31.6828 25.6502C30.7715 24.739 30.7715 23.2616 31.6828 22.3504C32.2832 21.75 33.1387 21.5406 33.9156 21.74C33.5442 22.4097 33.3327 23.1803 33.3327 24.0003ZM40.3327 24.0003C40.3327 25.289 39.288 26.3337 37.9993 26.3337C36.7107 26.3337 35.666 25.289 35.666 24.0003C35.666 22.7117 36.7107 21.667 37.9993 21.667C39.288 21.667 40.3327 22.7117 40.3327 24.0003Z"
                      fill="#0B081E"
                    />
                  </svg>

                  <div>
                    <div className="large-gray-text">Card</div>
                    <div style={{ fontWeight: "400", fontSize: "1.4rem" }}>
                      -
                    </div>
                  </div>
                  <div>
                    <div className="large-gray-text">Valid thru</div>
                    <div style={{ fontWeight: "400", fontSize: "1.4rem" }}>
                      -
                    </div>
                  </div>
                </div>
                <button
                  type="button"
                  className="payment-method-button"
                  onClick={(event) => handleShowModal(event, "addCard")}
                >
                  Add Payment Method
                </button>
              </div>
            )}
          </div>
          <div className="payment-section-bottom-container">
            <BuyCard />
            <BuyCard />
            <BuyCard />
          </div>
        </div>
        <div className="creator-section">
          <div className="bg-contact-estimate-container">
            <button
              type="button"
              className="contact-estimate-container"
              onClick={(event) => handleShowModal(event, "estimateModal")}
            >
              <p>Estimate how many credits you will use </p>
              <svg
                width="12"
                height="10"
                viewBox="0 0 12 10"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M10.2929 5.5H0.5C0.223858 5.5 0 5.27614 0 5C0 4.72386 0.223858 4.5 0.5 4.5H10.2929L7.14645 1.35355C6.95118 1.15829 6.95118 0.841709 7.14645 0.646447C7.34171 0.451184 7.65829 0.451184 7.85355 0.646447L11.8536 4.64645C12.0488 4.84171 12.0488 5.15829 11.8536 5.35355L7.85355 9.35355C7.65829 9.54882 7.34171 9.54882 7.14645 9.35355C6.95118 9.15829 6.95118 8.84171 7.14645 8.64645L10.2929 5.5Z"
                  fill="#454354"
                />
              </svg>
            </button>
            <button
              className="contact-estimate-container"
              onClick={(event) => handleShowModal(event, "customPack")}
            >
              <p>Contact Us for custom packs</p>
              <svg
                width="12"
                height="10"
                viewBox="0 0 12 10"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M10.2929 5.5H0.5C0.223858 5.5 0 5.27614 0 5C0 4.72386 0.223858 4.5 0.5 4.5H10.2929L7.14645 1.35355C6.95118 1.15829 6.95118 0.841709 7.14645 0.646447C7.34171 0.451184 7.65829 0.451184 7.85355 0.646447L11.8536 4.64645C12.0488 4.84171 12.0488 5.15829 11.8536 5.35355L7.85355 9.35355C7.65829 9.54882 7.34171 9.54882 7.14645 9.35355C6.95118 9.15829 6.95118 8.84171 7.14645 8.64645L10.2929 5.5Z"
                  fill="#454354"
                />
              </svg>
            </button>
          </div>
          <div className="creator-profile-section">
            <div>
              <image src="image.jpg" alt="profile-image" />
              <div>Henry Garden</div>
              <div className="small-gray-text">Head of Fashion, Shopify</div>
            </div>
            <div className="best-ever-used-footer-container">
              <div className="best-ever-used-footer">
                The Best I Ever Used...
              </div>
              <div className="stars-container">
                {[1, 2, 3, 4, 5].map((star) => {
                  if (star !== 5) {
                    return (
                      <svg
                        key={star}
                        width="20"
                        height="20"
                        viewBox="0 0 20 20"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M9.45687 1.15702C9.67282 0.696972 10.3272 0.696972 10.5431 1.15703L12.9541 6.29305C13.039 6.474 13.2085 6.60076 13.4061 6.63113L18.8466 7.46732C19.3292 7.54148 19.5259 8.1302 19.1848 8.47952L15.2116 12.5488C15.0785 12.6851 15.0181 12.8765 15.0488 13.0645L15.9811 18.7803C16.0617 19.2747 15.5369 19.6444 15.0985 19.402L10.2904 16.7431C10.1097 16.6432 9.89032 16.6432 9.70965 16.7431L4.90147 19.402C4.46313 19.6444 3.93831 19.2747 4.01894 18.7803L4.95123 13.0645C4.9819 12.8765 4.92145 12.6851 4.78836 12.5488L0.815216 8.47952C0.474145 8.1302 0.670823 7.54148 1.15337 7.46732L6.59391 6.63113C6.79148 6.60076 6.96095 6.474 7.0459 6.29305L9.45687 1.15702Z"
                          fill="#6672FB"
                        />
                      </svg>
                    );
                  } else {
                    return (
                      <svg
                        key={star}
                        width="18"
                        height="19"
                        viewBox="0 0 18 19"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M8.54739 1.14084C8.72735 0.757465 9.27265 0.757464 9.45261 1.14084L11.4109 5.31252C11.6232 5.76489 12.0469 6.0818 12.5409 6.15771L17.0018 6.84333C17.4039 6.90514 17.5678 7.39573 17.2836 7.68684L13.995 11.0549C13.6623 11.3957 13.5111 11.8743 13.5878 12.3443L14.355 17.0477C14.4221 17.4596 13.9848 17.7677 13.6195 17.5657L9.72589 15.4126C9.27419 15.1628 8.72581 15.1628 8.27411 15.4126L4.38049 17.5657C4.01521 17.7677 3.57785 17.4596 3.64505 17.0477L4.4122 12.3443C4.48887 11.8743 4.33775 11.3957 4.00502 11.0549L0.716446 7.68683C0.43222 7.39573 0.596118 6.90514 0.998242 6.84333L5.45914 6.15771C5.95306 6.0818 6.37675 5.76489 6.5891 5.31252L8.54739 1.14084Z"
                          stroke="#0B081E"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                      </svg>
                    );
                  }
                })}
              </div>
              <div className="testimonial-text">
                It is a long established fact that a reader will be distracted
                by the readable content of a page when looking at its layout.
                The point of using Lorem Ipsum is that it has a more-or-less
                normal distribution of letters, as opposed to using 'Content
                here, content here', us versions have evolved over the years,
                sometimes by accident, sometimes on purpose (injected humour and
                the like).
              </div>
              <div className="large-gray-text">24th Mar 2020-11:45PM</div>
            </div>
          </div>
        </div>
      </div>
      {showHideModal && <Modal Ref={modalRef} modalState={modalState}>{renderModal()}</Modal>}
    </div>
  );
};

export default App;
