export default function listenForOutsideClick(
    listening,
    setListening,
    modalRef,
    setShowHideModal
  ) {
    return () => {
      if (listening) return;
      if (!modalRef.current) return;
      setListening(true);
      [`click`, `touchstart`].forEach((type) => {
        document.addEventListener(`click`, (evt) => {
          const cur = modalRef.current;
          const node = evt.target;
          if (cur.contains(node)) return;
          setShowHideModal(false);
        });
      });
    };
  }
  