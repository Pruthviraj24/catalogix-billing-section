import {useState} from "react"


const estimateCreditProducts = [
    {
        id:1,
        product:'SKU',
        time:'6.5 Hours',
        credits:'15 Credits'
    },{
        id:2,
        product:'Image tilte',
        time:'6.5 Hours',
        credits:'15 Credits'
    },{
        id:3,
        product:'HSN code',
        time:'6.5 Hours',
        credits:'15 Credits'
    },{
        id:4,
        product:'Product title',
        time:'6.5 Hours',
        credits:'15 Credits'
    },{
        id:5,
        product:'Image Title-Amazon',
        time:'6.5 Hours',
        credits:'15 Credits'
    },{
        id:6,
        product:'Category',
        time:'6.5 Hours',
        credits:'15 Credits'
    }
]

const EstimateModal = ()=>{
    const [selectAll, setSelectAll] = useState(false)


    const  handleOnSelectCredit = ()=>{

    }

    return(
        <div>
            <h2>Estimate Credit Calc.</h2>
            <div className="hr-line"></div> 
            <div className=''>
                <p>No. of Products Process</p>
                <div className='products-count-container'>
                    <div>169020</div>
                    <div className="small-gray-text"> Total Products Count</div>
                </div>
            </div>

       
            <div className='estimate-navbar'>
                <div className='estimate-navbar-sub-container'>
                    <div  onClick={()=>setSelectAll(!selectAll)} value={selectAll}>
                        <div className='checkbox-svg-container'>
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.0865176 5.84128C0.211769 2.7161 2.71671 0.211356 5.8419 0.0863482L8.0006 0L10.1584 0.0863135C13.284 0.211336 15.7891 2.71662 15.9139 5.84218L16 8L15.9138 10.158C15.7891 13.2835 13.2841 15.7887 10.1586 15.9139L8 16.0003L5.84165 15.9139C2.71656 15.7887 0.211774 13.284 0.0865269 10.159L0 8L0.0865176 5.84128Z" fill="#0E113C"/>
                            </svg>
                            {selectAll ? 
                            <div className="tick-untick">
                                <svg width="8" height="6" viewBox="0 0 8 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1.59961 3.26706L3.01137 4.60039L6.39961 1.40039" stroke="white" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </div>
                            :
                            <div className="tick-untick">
                                <svg width="6" height="2" viewBox="0 0 6 2" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 1H5" stroke="white" stroke-width="1.6" stroke-linecap="round"/>
                                </svg>
                            </div>}
                        </div>
                    </div>
                    <div> Automation</div>
                </div>
                <div className='estimate-navbar-sub-container'>
                    <div>Est.Time Saved</div>
                    <div>Credit Needed</div>
                </div>
            </div>
            <div className='estimate-list-container'>
                    {
                        estimateCreditProducts.map(eachProduct => {
                    return(
                        <div key={eachProduct.id}>
                            <div className='estimate-list' >
                                    <div className='estimate-navbar-sub-container'>
                                    <div  onClick={()=>handleOnSelectCredit()} >
                                        <div className='checkbox-svg-container'>
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M0.0865176 5.84128C0.211769 2.7161 2.71671 0.211356 5.8419 0.0863482L8.0006 0L10.1584 0.0863135C13.284 0.211336 15.7891 2.71662 15.9139 5.84218L16 8L15.9138 10.158C15.7891 13.2835 13.2841 15.7887 10.1586 15.9139L8 16.0003L5.84165 15.9139C2.71656 15.7887 0.211774 13.284 0.0865269 10.159L0 8L0.0865176 5.84128Z" fill="#0E113C"/>
                                            </svg>
                                            {selectAll ? 
                                            <div className="tick-untick">
                                                <svg width="8" height="6" viewBox="0 0 8 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1.59961 3.26706L3.01137 4.60039L6.39961 1.40039" stroke="white" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
                                                </svg>
                                            </div>
                                            :
                                            <div className="tick-untick">
                                                <svg width="6" height="2" viewBox="0 0 6 2" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M1 1H5" stroke="white" stroke-width="1.6" stroke-linecap="round"/>
                                                </svg>
                                            </div>}
                                        </div>
                                    </div>
                                        <div>{eachProduct.product}</div>
                                    </div>
                                    <div className='estimate-list-sub-container'>
                                        <div>{eachProduct.time}</div>
                                        <div>{eachProduct.credits}</div>
                                    </div>
                                </div>
                            <div className="hr-line"></div> 
                        </div>)
                    })
                    }
            </div>
            <div className='estimate-modal-summary'>
                <div style={{display:'flex',alignItems:'center',gap:'1.4rem'}}>
                    <div>
                        <svg width="44" height="44" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M18.2949 9.26967C18.3214 9.24303 18.3444 9.21415 18.3637 9.18364L20.2692 7.27519C20.2896 7.25472 20.3079 7.23293 20.3241 7.21008C20.7435 6.76294 21.0004 6.16147 21.0004 5.5C21.0004 4.11929 19.8811 3 18.5004 3C17.8343 3 17.229 3.26055 16.7809 3.6853C16.7585 3.70124 16.7372 3.71925 16.7172 3.7393L3.74975 16.7203C2.77001 17.7016 2.77099 19.2898 3.75227 20.2729C4.73215 21.2506 6.3179 21.2488 7.29486 20.2697L18.1109 9.43687C18.1743 9.38472 18.2357 9.32899 18.2949 9.26967ZM15.5154 6.35733C15.4984 6.38184 15.4791 6.40517 15.4573 6.42699C14.8676 7.01756 14.8682 7.97354 15.4593 8.56574C16.0015 9.10667 16.8532 9.14811 17.4428 8.6907L18.1559 7.97645C17.0519 7.82428 16.178 6.95128 16.0244 5.84774L15.5154 6.35733ZM14.0388 7.83547L4.45731 17.427C3.86763 18.0176 3.86822 18.9735 4.45931 19.5657C5.0475 20.1526 6.00009 20.1515 6.58709 19.5632L16.157 9.97849C15.6432 9.90317 15.1483 9.66803 14.7523 9.27291C14.3483 8.86813 14.1104 8.36079 14.0388 7.83547ZM17.0004 16H17.5004C17.7766 16 18.0004 16.2239 18.0004 16.5C18.0004 16.7761 17.7766 17 17.5004 17H17.0004V17.5616C17.0004 17.8377 16.7766 18.0616 16.5004 18.0616C16.2243 18.0616 16.0004 17.8377 16.0004 17.5616V17H15.5004C15.2243 17 15.0004 16.7761 15.0004 16.5C15.0004 16.2239 15.2243 16 15.5004 16H16.0004V15.5C16.0004 15.2239 16.2243 15 16.5004 15C16.7766 15 17.0004 15.2239 17.0004 15.5V16ZM10.0004 3.5V4H9.50043C9.22429 4 9.00043 4.22386 9.00043 4.5C9.00043 4.77614 9.22429 5 9.50043 5H10.0004V5.5C10.0004 5.77614 10.2243 6 10.5004 6C10.7766 6 11.0004 5.77614 11.0004 5.5V5H11.5004C11.7766 5 12.0004 4.77614 12.0004 4.5C12.0004 4.22386 11.7766 4 11.5004 4H11.0004V3.5C11.0004 3.22386 10.7766 3 10.5004 3C10.2243 3 10.0004 3.22386 10.0004 3.5ZM22.0004 12H22.5004C22.7766 12 23.0004 12.2239 23.0004 12.5C23.0004 12.7761 22.7766 13 22.5004 13H22.0004V13.5C22.0004 13.7761 21.7766 14 21.5004 14C21.2243 14 21.0004 13.7761 21.0004 13.5V13H20.5004C20.2243 13 20.0004 12.7761 20.0004 12.5C20.0004 12.2239 20.2243 12 20.5004 12H21.0004V11.5C21.0004 11.2239 21.2243 11 21.5004 11C21.7766 11 22.0004 11.2239 22.0004 11.5V12ZM20.0004 5.5C20.0004 6.32843 19.3289 7 18.5004 7C17.672 7 17.0004 6.32843 17.0004 5.5C17.0004 4.67157 17.672 4 18.5004 4C19.3289 4 20.0004 4.67157 20.0004 5.5Z" fill="white"/>
                        </svg>
                    </div>
                    <div >
                        <div>
                            5 Automation
                        </div>
                        <div>Total</div>
                    </div>
                </div>
                <div style={{display:'flex',alignItems:'center',gap:'3rem'}}>
                    <div >
                        <div>24</div>
                        <div>Hours</div>
                    </div>
                    <div>
                        <div>124682</div>
                        <div>Credits Req</div>
                    </div>
                </div>
            </div> 
        </div>
    )
}

export default EstimateModal