import "../../App.scss";
import { useState } from "react";

const BillingInformationModal = (props) => {
    const [country,setCountry] = useState('India')
    const [address,setAddress] = useState('')
    const [postalCode,setPostalCode] = useState('')
    const [city,setCity] = useState('')
    const [state,setState] = useState('Karnataka')
    const [phoneNumber,setPhoneNumber] = useState('')
    const [defaultAddress,setDefaultAddress] = useState(false)


  const { changeModalContent } = props;


  const handleBillingInformationSubmition = (event)=>{
    event.preventDefault()

    const billingDetalis = {
        country,
        address,
        postalCode,
        city,
        state,
        phoneNumber,
        defaultAddress
    }
    console.log(billingDetalis);

  }


  return (
    <div style={{width:'100%'}}>
      <div
        className="modal-arrow-heading"
        onClick={(event) => changeModalContent(event, "paymentMethod")}
      >
        <div>
          <svg
            width="6"
            height="12"
            viewBox="0 0 6 12"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M5.53116 11.2871C5.8736 10.9935 5.91326 10.478 5.61974 10.1355L2.07529 6.00034L5.61974 1.86515C5.91326 1.5227 5.8736 1.00714 5.53116 0.713611C5.18871 0.420083 4.67314 0.459742 4.37962 0.802191L0.379615 5.46886C0.117472 5.77469 0.117472 6.22598 0.379615 6.53182L4.37962 11.1985C4.67314 11.5409 5.18871 11.5806 5.53116 11.2871Z"
              fill="#0B081E"
              stroke="#0B081E"
              stroke-width="0.3"
            />
          </svg>
        </div>
        <div>
          <h2>Billing information</h2>
        </div>
      </div>
      <div className="hr-line"></div> 
      <form onSubmit={handleBillingInformationSubmition}>
      <div>
        <div className="drop-down-container">
          <label>Country</label>
          <select id='countries' className="modal-drop-down" value={country} onChange={(e)=>setCountry(e.target.value)}>
            <option value='India'>India</option>
            <option value='Sri Lanka'>Sri Lanka</option>
            <option value='Buthan'>Buthan</option>
            <option value='Nepal'>Nepal</option>
          </select>
        </div>
      </div>
      <div>
        <div className="address-textarea-container">
          <label>Address</label>
          <textarea
            className="address-textarea"
            placeholder="Your address"
            value={address}
            onChange={(e)=>setAddress(e.target.value)}
          ></textarea>
        </div>
        <div className="regular-input-container">
          <label>Postal Code</label>
          <input
            type="text"
            className="regular-modal-input"
            placeholder="56789"
            value={postalCode}
            onChange={(e)=>setPostalCode(e.target.value)}
          />
        </div>
        <div className="regular-input-container">
          <label>City</label>
          <input
            type="text"
            className="regular-modal-input"
            placeholder="E.g:Banglore"
            value={city}
            onChange={(e)=>setCity(e.target.value)}
          />
        </div>
        <div className="drop-down-container">
          <label>State</label>
          <select id='states' className="modal-drop-down" value={state} onChange={(e)=>setState(e.target.value)} >
            <option>Karnataka</option>
            <option>Kerala</option>
            <option>Andrapradesh</option>
            <option>Tamil Nadu</option>
          </select>
        </div>
        <div className="regular-input-container">
          <label>Phone Number</label>
          <input
            type="text"
            className="regular-modal-input"
            placeholder="1234567890"
            value={phoneNumber}
            onChange={(e)=>setPhoneNumber(e.target.value)}
          />
        </div>
        <div className="default-payment-input-contianer" onClick={()=>setDefaultAddress(!defaultAddress)}>
          <div className='checkbox-svg-container'>
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.0865176 5.84128C0.211769 2.7161 2.71671 0.211356 5.8419 0.0863482L8.0006 0L10.1584 0.0863135C13.284 0.211336 15.7891 2.71662 15.9139 5.84218L16 8L15.9138 10.158C15.7891 13.2835 13.2841 15.7887 10.1586 15.9139L8 16.0003L5.84165 15.9139C2.71656 15.7887 0.211774 13.284 0.0865269 10.159L0 8L0.0865176 5.84128Z" fill="#0E113C"/>
                </svg>
                {defaultAddress ? 
                <div className="tick-untick">
                    <svg width="8" height="6" viewBox="0 0 8 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.59961 3.26706L3.01137 4.60039L6.39961 1.40039" stroke="white" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
                :
                <div className="tick-untick">
                    <svg width="6" height="2" viewBox="0 0 6 2" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1 1H5" stroke="white" stroke-width="1.6" stroke-linecap="round"/>
                    </svg>
                </div>}
            </div>
          <div >Shipping Address same as Billing Address</div>
        </div>
        <button className="add-card-button" type="submit">Save Address</button>
      </div>
      </form>
    </div>
  );
};

export default BillingInformationModal;
