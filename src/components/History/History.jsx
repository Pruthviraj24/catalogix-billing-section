import "../../App.scss"

const purchaseHistory = [
    {
        id:1,
        date:new Date(),
        description:'Brought - 5467',
        price:'$19.99',
        creditsBrouht:100
    },
    {
        id:2,
        date:new Date(),
        description:'Brought - 5467',
        price:'$19.99',
        creditsBrouht:100
    },
    {
        id:3,
        date:new Date(),
        description:'Brought - 5467',
        price:'$19.99',
        creditsBrouht:100
    },
    {
        id:4,
        date:new Date(),
        description:'Brought - 5467',
        price:'$19.99',
        creditsBrouht:100
    },
    {
        id:5,
        date:new Date(),
        description:'Brought - 5467',
        price:'$19.99',
        creditsBrouht:100
    }
]


const History = ()=>{


    return(
        <div>
            <div className="history-modal-header">
                <div>Purchase History</div>
                    <div className="hr-line"></div>
                <div className="export-button">Export</div>
            </div>
            <div className="history-navbar">
                <div className="history-nav-sub1-container">
                    <div>Date</div>
                    <div style={{height:'18px'}}>Description</div>
                </div>
                <div className="history-nav-sub2-container">
                    <div>Price</div>
                    <div>Credits Brought</div>
                </div>
            </div>
            <div className="history-modal-list-container">
                    {
                        purchaseHistory.map(purchase => {
                            return(
                                <>  
                                    <div key={purchase.id} className='history-list-container'>
                                        <div className="history-list-sub1-container">
                                            <div> 12th Dec ‘21 - 5:00 PM</div>
                                            <div>{purchase.description}</div>
                                        </div>
                                        <div className="history-list-sub2-container">
                                            <div>{purchase.price}</div>
                                            <div style={{color:'#35CB44',fontSize:'1.6rem'}}>{purchase.creditsBrouht}</div>
                                        </div>                                
                                    </div>
                                    <div className="hr-line"></div> 
                                </>
                            )
                        })
                    }
                    <div className="download-container">
                        <svg width="92" height="88" viewBox="0 0 92 88" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g filter="url(#filter0_d_5773_583215)">
                            <path d="M28 40C28 31.1634 35.1634 24 44 24H72V64H44C35.1634 64 28 56.8366 28 48V40Z" fill="#FCFCFF"/>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M60.5 47C60.2239 47 60 47.2239 60 47.5V50.5C60 51.3284 59.3284 52 58.5 52H46.5C45.6716 52 45 51.3284 45 50.5V47.5C45 47.2239 44.7761 47 44.5 47C44.2239 47 44 47.2239 44 47.5V50.5C44 51.8807 45.1193 53 46.5 53H58.5C59.8807 53 61 51.8807 61 50.5V47.5C61 47.2239 60.7761 47 60.5 47ZM56.1464 43.1464L53 46.2929V35.5C53 35.2239 52.7761 35 52.5 35C52.2239 35 52 35.2239 52 35.5V46.2929L48.8536 43.1464C48.6583 42.9512 48.3417 42.9512 48.1464 43.1464C47.9512 43.3417 47.9512 43.6583 48.1464 43.8536L52.1464 47.8536C52.3417 48.0488 52.6583 48.0488 52.8536 47.8536L56.8536 43.8536C57.0488 43.6583 57.0488 43.3417 56.8536 43.1464C56.6583 42.9512 56.3417 42.9512 56.1464 43.1464Z" fill="#0B081E"/>
                            <path d="M28.5 40C28.5 31.4396 35.4396 24.5 44 24.5H71.5V63.5H44C35.4396 63.5 28.5 56.5604 28.5 48V40Z" stroke="#F0F1FF"/>
                            </g>
                            <defs>
                            <filter id="filter0_d_5773_583215" x="0" y="0" width="92" height="88" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                            <feOffset dx="-4"/>
                            <feGaussianBlur stdDeviation="12"/>
                            <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.08 0"/>
                            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_5773_583215"/>
                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_5773_583215" result="shape"/>
                            </filter>
                            </defs>
                        </svg>
                    </div>
            </div>
        </div>
    )
}

export default History