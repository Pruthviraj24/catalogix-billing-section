import '../../App.scss'


const Modal = (props)=>{
    
    let classList;

    switch(props.modalState){
        case 'purchaseHistory':
            classList = 'history-modal'
            break;
        case 'usageHistory':
            classList = 'history-modal'
            break;
        case 'estimateModal':
            classList = 'estimate-modal'
            break;
        case 'paymentMethod':
            classList = 'payment-method-modal'
            break;
        case 'addCard':
            classList ='add-card-modal'
            break;
        case 'billingInformation':
            classList = 'billing-info-modal'
            break;
        case 'customPack':
            classList = 'custom-pack-modal'
            break;
        default:
            classList = null
    }

    return(
        <div className={`${classList} modal-container`}  onClick={(event)=>event.stopPropagation()}>
            {props.children}
        </div>
    )
}

export default Modal