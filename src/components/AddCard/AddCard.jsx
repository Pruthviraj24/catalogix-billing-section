import "../../App.scss";
import { useState } from "react";

const AddCard = (props) => {
  const { changeModalContent, close, isCardEmpty } = props;
  const [cardNumber, setCardNumber] = useState("");
  const [expDate ,setExpDate] = useState('')
  const [cvv,setCvv] = useState('')
  const [country,setCountry] = useState('India')
  const [useDefault,setDefaultCard] = useState(false)


  const handleSumbitCardDetails = (event)=>{
    event.preventDefault()
    if(cardNumber.length !== 16) return

    const cardDetails = {
        cardNumber,
        expiryDate:expDate,
        cvv,
        country,
        useDefault
    }

    console.log(cardDetails);

  }



  return (
    <div>
      <div
        className="modal-arrow-heading"
        onClick={(event) =>
          isCardEmpty ? changeModalContent(event, "paymentMethod") : close()
        }
      >
        <div>
          <svg
            width="6"
            height="12"
            viewBox="0 0 6 12"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M5.53116 11.2871C5.8736 10.9935 5.91326 10.478 5.61974 10.1355L2.07529 6.00034L5.61974 1.86515C5.91326 1.5227 5.8736 1.00714 5.53116 0.713611C5.18871 0.420083 4.67314 0.459742 4.37962 0.802191L0.379615 5.46886C0.117472 5.77469 0.117472 6.22598 0.379615 6.53182L4.37962 11.1985C4.67314 11.5409 5.18871 11.5806 5.53116 11.2871Z"
              fill="#0B081E"
              stroke="#0B081E"
              stroke-width="0.3"
            />
          </svg>
        </div>
        <div>
          <h2>Add Card</h2>
        </div>
      </div>
      <div className="hr-line"></div> 
      <form className="add-card-form-container" onSubmit={handleSumbitCardDetails}>
        <div className="card-number-input-container">
          <label>Card Number</label>
          <input
            type="text"
            value={cardNumber}
            onChange={(e) => setCardNumber(e.target.value)}
            className="card-number-input"
            placeholder="1234 1234 1234 1234"
          />
        </div>
        <div className="add-card-expiry-cvv-container">
          <div className="expiry-cvv-input-container">
            <label>Expiry</label>
            <input
              type="text"
              value={expDate}
              onChange={(e) => setExpDate(e.target.value)}
              placeholder="MM/YY"
              className="card-expiry-cvv-input"
            />
          </div>
          <div className="expiry-cvv-input-container">
            <label>CVV</label>
            <input
              type="text"
              value={cvv}
              onChange={(e) => setCvv(e.target.value)}
              placeholder="CVV"
              className="card-expiry-cvv-input"
            />
          </div>
        </div>
        <div className="drop-down-container">
          <label>Country</label>
          <select className="modal-drop-down" value={country} onChange={(e)=>setCountry(e.target.value)}>
            <option value='India'>India</option>
            <option value='Sri Lanka'>Sri Lanka</option>
            <option value='Buthan'>Buthan</option>
            <option value='Nepal'>Nepal</option>
          </select>
        </div>
        <div className="default-payment-input-contianer" onClick={()=>setDefaultCard(!useDefault)}>
        <div >
            <div className='checkbox-svg-container'>
                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.0865176 5.84128C0.211769 2.7161 2.71671 0.211356 5.8419 0.0863482L8.0006 0L10.1584 0.0863135C13.284 0.211336 15.7891 2.71662 15.9139 5.84218L16 8L15.9138 10.158C15.7891 13.2835 13.2841 15.7887 10.1586 15.9139L8 16.0003L5.84165 15.9139C2.71656 15.7887 0.211774 13.284 0.0865269 10.159L0 8L0.0865176 5.84128Z" fill="#0E113C"/>
                </svg>
                {useDefault ? 
                <div className="tick-untick">
                    <svg width="8" height="6" viewBox="0 0 8 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.59961 3.26706L3.01137 4.60039L6.39961 1.40039" stroke="white" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
                :
                <div className="tick-untick">
                    <svg width="6" height="2" viewBox="0 0 6 2" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1 1H5" stroke="white" stroke-width="1.6" stroke-linecap="round"/>
                    </svg>
                </div>}
            </div>
          </div>
          <div>Use as default payment method</div>
        </div>
        <button className="add-card-button" type='submit'>Add Card</button>
      </form>
      <div>
        You can review importent information form Typographic on their Terms of
        Service and Privacy Policy pages
      </div>
    </div>
  );
};

export default AddCard;
