import { useState } from "react";



const BuyCard = () => {
  const [recurringPurchase,setRecurringPurchase] = useState(false)


  return (
    <div className="buy-card">
    <div style={{height:'170px'}}>
    <div className="buy-card-top-cantainer">
        <div>
          <div className="available-credit-count">100</div>
          <div className="small-gray-text">Credits Pack</div>
        </div>
        <div>
          <div className="used-credit-count">$19.99</div>
          <div className="small-gray-text" style={{textAlign:'end'}}>USD</div>
        </div>
      </div>
        <button type="button" className="buy-button">
          Buy
        </button>
    </div>
      <div className="recurring-raido-input-container">
           <div>
            <input id='set-recurring' type="radio" value={recurringPurchase} onChange={()=>setRecurringPurchase(!recurringPurchase)}/>
           </div> 
            <label htmlFor='set-recurring'>Set Recurring Purchase</label>
      </div>
    </div>
  );
};

export default BuyCard;
