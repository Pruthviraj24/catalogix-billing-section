// import {AiOutlineHome,AiOutlineTag} from "react-icons/ai"
// import {MdOutlineKeyboardArrowUp} from "react-icons/md"
// import {MdOutlineKeyboardArrowDown} from "react-icons/md"
// import {FcGallery} from "react-icons/fc"
// import {BsCodeSlash} from "react-icons/bs"
// import {FiPlayCircle} from "react-icons/fi"
// import {CgProfile} from "react-icons/cg"
// import {TbBuildingStore,TbArrowBarLeft,TbDiscount2} from "react-icons/tb"
import SideNavItems from "../SideNavItems/SideNavItems"
import "./styles.css"


const topNavItems = [
    {
        id:1,
        name:"Products",
        icon:'AiOutlineTag'
    },
    {
        id:2,
        name:"Assets",
        icon:'FcGallery'
    },
    {
        id:3,
        name:"Discounts",
        icon:'TbDiscount2'
    }
]


const bottomNavItems = [
    {
        id:1,
        name:"Dev Settings",
        icon:'BsCodeSlash'
    },
    {
        id:2,
        name:"Preview Site",
        icon:'FiPlayCircle'
    },
    {
        id:3,
        name:"Store Settings",
        icon:'TbBuildingStore'
    },
    {
        id:4,
        name:"My Profile",
        icon:'CgProfile'
    }
]




const SideNavbar = ()=>{




    return(
        <div className="side-navbar">
            <div>
            <div className="side-nav-store-header">
                <div className="home-icon-cat-store-container">
                    <div className="side-nav-home-container">
                        {/* <AiOutlineHome /> */}
                    </div>
                    <div className="catalogix-text-container">
                        <p className="catalogix-header">Catalogix WS</p>
                        <h3 className="store-header">Store 01</h3>
                    </div>
                </div>
             
                <div className="up-down-arrow-button-container">
                    <button className="up-down-arrow-buttons">
                        {/* <MdOutlineKeyboardArrowUp/> */}
                    </button>
                    <button className="up-down-arrow-buttons">
                        {/* <MdOutlineKeyboardArrowDown/> */}
                    </button>
                </div>
            </div>
            <div className="side-navitems-top-list-container">
                {
                    topNavItems.map(eachItem => <SideNavItems items = {eachItem} key={eachItem.id}/>)
                }
            </div>
            </div>
            <div>
            <div className="side-navitems-bottom-list-container" style={{marginTop:"560px"}}>
                {
                    bottomNavItems.map(eachItem => <SideNavItems items = {eachItem} key={eachItem.id}/>)
                }
            </div>
            <div className="collapse-container">
                <div className="collapse-icon-container">
                    {/* <TbArrowBarLeft/> */}
                </div>
                <p>Collapse</p>
            </div>
            </div>
        </div>
    )
}

export default SideNavbar