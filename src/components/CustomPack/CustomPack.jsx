import {useState} from "react"

const CustomPack = (props)=>{
    const {close,changeModalContent} = props
    const [creditsNeeded,setCreditsNeeded] = useState('')
    const [storageNeeded,setStorageNeeded] = useState('')

    const handleCustomRequest = (event)=>{
        event.preventDefault()
        console.log({
            creditsNeeded,
            storageNeeded
        });
        changeModalContent(event,'requestCustom')
    }

    return(
        <div>
            <div className='modal-arrow-heading' onClick={()=>close()}>
                <div>
                    <svg width="6" height="12" viewBox="0 0 6 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.53116 11.2871C5.8736 10.9935 5.91326 10.478 5.61974 10.1355L2.07529 6.00034L5.61974 1.86515C5.91326 1.5227 5.8736 1.00714 5.53116 0.713611C5.18871 0.420083 4.67314 0.459742 4.37962 0.802191L0.379615 5.46886C0.117472 5.77469 0.117472 6.22598 0.379615 6.53182L4.37962 11.1985C4.67314 11.5409 5.18871 11.5806 5.53116 11.2871Z" fill="#0B081E" stroke="#0B081E" stroke-width="0.3"/>
                    </svg>
                </div>
                <div >
                    <h2>Custom Pack Request</h2>
                </div>
            </div>
            <div className="hr-line"></div> 
            <form onSubmit={handleCustomRequest}>
                <div className="regular-input-container">
                    <label>Approx. Credits Needed</label>
                    <input type='text' className="regular-modal-input" placeholder="56789" value={creditsNeeded} onChange={(e)=>setCreditsNeeded(e.target.value)}/>
                </div>
                <div className="regular-input-container">
                    <label>Approx. Storage Needed</label>
                    <input type='text' className="regular-modal-input" placeholder="3000GB" value={storageNeeded} onChange={(e)=>setStorageNeeded(e.target.value)}/>
                </div>
                <button className='add-card-button' type='submit'>
                    Submit
                 </button>
            </form>
        </div>
    )
}

export default CustomPack