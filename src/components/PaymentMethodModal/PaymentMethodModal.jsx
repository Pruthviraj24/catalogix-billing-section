import "../../App.scss";

const PaymentMethodModal = (props) => {
  const { close, changeModalContent } = props;

  return (
    <div>
      <div className="modal-arrow-heading" onClick={() => close()}>
        <div>
          <svg
            width="6"
            height="12"
            viewBox="0 0 6 12"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M5.53116 11.2871C5.8736 10.9935 5.91326 10.478 5.61974 10.1355L2.07529 6.00034L5.61974 1.86515C5.91326 1.5227 5.8736 1.00714 5.53116 0.713611C5.18871 0.420083 4.67314 0.459742 4.37962 0.802191L0.379615 5.46886C0.117472 5.77469 0.117472 6.22598 0.379615 6.53182L4.37962 11.1985C4.67314 11.5409 5.18871 11.5806 5.53116 11.2871Z"
              fill="#0B081E"
              stroke="#0B081E"
              stroke-width="0.3"
            />
          </svg>
        </div>
        <div>
          <h2>Payment Method</h2>
        </div>
      </div>
      <div>
        <div className="payment-modal-saved-card">
          <div>
            <div className="large-gray-text">Mastercard</div>
            <div>4567</div>
          </div>
          <div>
            <div className="large-gray-text">Valid thru</div>
            <div>08/28</div>
          </div>
          <button className="payment-modal-default">Default</button>
        </div>
        <div
          className="add-payment-method"
          onClick={(event) => changeModalContent(event, "addCard")}
        >
          Add Payment Method
        </div>
        <div className="hr-line"></div> 

        <div className="billing-information">
          <h3>Billing Information</h3>
          <div className="hr-line"></div> 
          <p>1600 Amphitheatre Parkway Montain View, CA 94043 United States</p>
          <div className="small-gray-text">
            Shipping Address Same as Billing Address
          </div>
          <div
            className="update-information-container"
            onClick={(event) => changeModalContent(event, "billingInformation")}
          >
            <div>
              <svg
                width="16"
                height="18"
                viewBox="0 0 16 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  clip-rule="evenodd"
                  d="M0 15.5V12.5C0 12.3674 0.0526784 12.2402 0.146447 12.1464L12.1464 0.146447C12.3417 -0.0488155 12.6583 -0.0488155 12.8536 0.146447L15.8536 3.14645C16.0488 3.34171 16.0488 3.65829 15.8536 3.85355L3.85355 15.8536C3.75979 15.9473 3.63261 16 3.5 16H0.5C0.223858 16 0 15.7761 0 15.5ZM3.29289 15L14.7929 3.5L12.5 1.20711L1 12.7071V15H3.29289ZM0 17.5C0 17.7761 0.223858 18 0.5 18H10.5069C10.7831 18 11.0069 17.7761 11.0069 17.5C11.0069 17.2239 10.7831 17 10.5069 17H0.5C0.223858 17 0 17.2239 0 17.5Z"
                  fill="#0B081E"
                />
              </svg>
            </div>
            <div>Update Information</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PaymentMethodModal;
